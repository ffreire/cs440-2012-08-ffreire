connected(alligatorpond,backdoor,2).
connected(backdoor,exit,1).
connected(bedroom,library,5).
connected(billiardroom,greenhouse,2).
connected(coatroom,servantroom,2).
connected(diningroom,alligatorpond,2).
connected(diningroom,kitchen,4).
connected(diningroom,library,7).
connected(entrance,mainhall,2).
connected(greenhouse,backdoor,5).
connected(kitchen,billiardroom,5).
connected(kitchen,greenhouse,1).
connected(library,billiardroom,3).
connected(mainhall,bedroom,4).
connected(mainhall,coatroom,3).
connected(mainhall,diningroom,3).
connected(mainhall,library,6).
connected(servantroom,alligatorpond,2).
connected(servantroom,diningroom,3).

pathfrom(A,B,Path) :-
       travel(A,B,[A],Q), 
       reverse(Q,Path).

travel(A,B,P,[B|P]) :- 
       connected(A,B,_).
travel(A,B,Visited,Path) :-
       connected(A,C,_),           
       C \== B,
       not(member(C,Visited)),
       travel(C,B,[C|Visited],Path).  

costof([],_).
costof([X,Y], C) :- connected(X,Y,C).
costof([X,Y|T], C) :-
        connected(X,Y,Z1),
        costof([Y|T],Z2),
        C is Z1+Z2.



costoflist([], _).

costoflist([XX], Ans) :-
    costof(XX,C1),
    Ans = [C1].

costoflist([XX|T], Ans) :-
    costof(XX,C1),
    costoflist(T, C2),
    merge([C1],C2,Ans2), 
    Ans = Ans2,
    !.

minlist(X,L) :- 
            member(X,L),
            list_min(L,Y),
            X==Y.

list_min([L|Ls], Min) :-
    list_min(Ls, L, Min).

list_min([], Min, Min).
list_min([L|Ls], Min0, Min) :-
    Min1 is min(L, Min0),
    list_min(Ls, Min1, Min).



merge([],L,L).  
merge([H|T],L,[H|M]) :- merge(T,L,M).

member2(A,B,[A|_],[B|_]).
member2(A,B,[_|X],[_|Y]) :- member2(A,B,X,Y).

mincost(P,C,PL,CL):- 
          member2(P,C,PL,CL),
          minlist(C, CL).

findpath(A,B,P,C):- 
            findall(X,pathfrom(A,B,X),PL),
            findall(X,costoflist(PL,X),CL),
            rempar(CL,CLO),
            list_min(CLO, MC),
            mincost(P, MC, PL, CLO),
            C = MC.

rempar([],_).
rempar([XX], P) :- 
            P = XX.

goodpath(L):- 
          not(member(alligatorpond,L)),
          member(servantroom,L);
          not(member(alligatorpond,L)),
          member(billiardroom,L).

findgoodpath(A,B,P,C):- 
            findall(X,(pathfrom(A,B,X), goodpath(X)),PL),
            findall(X,costoflist(PL,X),CL),
            rempar(CL,CLO),
            list_min(CLO, MC),
            mincost(P, MC, PL, CLO),
            C = MC.
