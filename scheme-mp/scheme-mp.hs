import Text.ParserCombinators.Parsec
import Data.Maybe
import Control.Monad

data Exp = IntExp Integer
         | SymExp String
         | SExp [Exp]
         deriving (Show)

data Val = IntVal Integer
         | SymVal String
         | PrimVal ([Val] -> Val)   
         | Closure [String] Exp [(String,Val)]
         | DefVal String Val      

run x = parseTest x

-- Lexicals

adigit = oneOf ['0'..'9']
digits = many1 adigit

asymbol = oneOf "-*+/:'?><=abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWZYX"
symbols = many1 asymbol

whitespace = skipMany1 space

-- Grammaticals

anInt = do d <- digits
           return $ IntExp (read d)

anSymbol = do s <- symbols
              return $ SymExp s

parens = do char '(' 
            skipMany space
            v <- sepBy anExp whitespace
            char ')'
            return $ SExp v

anAtom =  parens
      <|> anSymbol
      <|> anInt

anExp = anAtom

-- Evaluator
runtime = [("+", PrimVal $ liftIntOp (+) 0),
           ("*", PrimVal $ liftIntOp (*) 1),
           ("-", PrimVal $ liftIntOp (-) 0)]

eval :: Exp -> [(String,Val)] -> Val
eval (SymExp s) all@((st,vl):xs)       = let x = fromMaybe (error ("Symbol "++s++" has no value.")) (lookup s all) in 
                                         SymVal (show x) 
eval (IntExp i) env                    = IntVal i
eval (SExp []) env = SymVal "nil"
eval (SExp ((SymExp "define"):(SymExp fn):(SExp args):body:[])) env = DefVal fn closure
                                                                      where closure = (Closure (map (symExpString) args) body nenv)
                                                                            nenv = (fn, closure) : env
eval (SExp ((SymExp "def"):(SymExp var):body:[])) env = DefVal var (eval body env)
eval (SExp ((SymExp fn):args)) env = case lookup fn env of
       Just x -> apply fn (map (\a -> eval a env) args) env
       Nothing -> error ("Function " ++ fn ++ " not defined.")
-- Printer

instance Show Val where
  show (SymVal s) = s
  show (IntVal i) = show i
  show (DefVal s _) = s
  show (Closure _ _ _) = ""

repl defs =
  do putStr "> "
     l <- getLine
     case parse anExp "Expression" l of
       Right exp -> putStr (show x) >> putStrLn "" >> repl (modify x defs)
                      where x = (eval exp defs)
       Left pe   -> putStr (show pe) >> putStrLn "" >> repl defs


-- Functions
modify (DefVal s v) defs = (s, v) : defs 
modify _ defs            = defs

liftIntOp f a x = IntVal $ foldr f a (map (intValInt) x)

intValInt (IntVal x) = x

symExpString (SymExp x) = x

apply fn args env = case lookup fn env of
       Just (PrimVal fn) -> fn args
       Just (Closure cargs exp cenv) -> eval exp (zip cargs args ++ cenv)
       Nothing -> error ("Function " ++ fn ++ " not defined.")

evalfn (Closure cargs exp cenv) args env = eval exp (zip cargs (map (\a -> eval a env) args) ++ cenv)
