import Data.HashMap.Strict as H

-- Initial types

type ForthState = (IStack, CStack, Dictionary)

type IStack = [Integer]
initialIStack = []

type CStack = [[String]]
initialCStack = []

-- Type for the symbol dictionary

type Dictionary = H.HashMap String [Entry]

data Entry =
     Prim ([Integer] -> [Integer])
   | Def [String]
   | Num Integer
   | Unknown String

instance Show Entry where
  show (Prim f)    = "Prim"
  show (Def s)     = show s
  show (Num i)     = show i
  show (Unknown s) = "Unknown: " ++ s

-- Dictionary helpers

wrap2 f (x:y:xs) = (f x y):xs
wrap2 f _ = error "Value stack underflow!"

dlookup :: String -> Dictionary -> Entry
dlookup word dict =
  case H.lookup word dict of
    Nothing -> case reads word of
                 [(i,"")] -> Num i
                 _        -> Unknown word
    Just x  -> head x

dinsert :: String -> Entry -> Dictionary -> Dictionary
dinsert key val dict =
   case H.lookup key dict of
      Nothing -> H.insert key [val] dict
      Just x  -> H.insert key (val:x) dict

-- Initial Dictionary

dictionary = dinsert "+" (Prim $ wrap2 (+)) (dinsert "-" (Prim $ wrap2 (-)) (dinsert "*" (Prim $ wrap2 (*)) (dinsert "/" (Prim $ wrap2 (div)) H.empty)))

-- The Evaluator

eval :: [String] -> ForthState -> IO ForthState
eval []    (istack, [],     dict) = return (istack, [], dict)
eval words (istack, cstack, dict) =
  case dlookup (head words) dict of
    Num i        -> eval xs (i:istack, cstack, dict)
    Prim f       -> eval xs (f istack, cstack, dict) 
    Unknown "dup"  -> do { eval xs (dup istack, cstack, dict) }
    Unknown "drop"  -> do { eval xs (dropF istack, cstack, dict) }
    Unknown "swap"  -> do { eval xs (swap istack, cstack, dict) }   
    Unknown "rot"  -> do { eval xs (rot istack, cstack, dict) }
    Unknown "<"  -> do {  eval xs ((stA istack):(dropF (tail istack)), cstack, dict) }
    Unknown ">"  -> do {  eval xs ((gtA istack):(dropF (tail istack)), cstack, dict) }
    Unknown "="  -> do {  eval xs ((eqA istack):(dropF (tail istack)), cstack, dict) }
    Unknown ".S"  -> do { putStrLn $ showAll (reverse istack);
                             eval xs (istack, cstack, dict) }  
    Unknown "exit"  -> do { putStrLn $ show (head istack);
                             eval xs (tail istack, cstack, dict) }
    Unknown "if" -> do { eval (condition istack xs) (istack, cstack, dict); }       
    Unknown "else" -> do { eval (conditionN istack xs) (istack, cstack, dict); }
    Unknown "begin" -> do { eval (untilV xs "again") (istack, cstack, dict); }                                              
    Unknown ":"  -> do { eval (indexOf xs ";") (istack, cstack, (dinsert (head xs) (Def $ (untilV (tail xs) ";")) dict)) }
    Def d        -> do { eval (d++xs) (istack, cstack, dict) }
    Unknown "."  -> do { putStrLn $ show (head istack);
                             eval xs (tail istack, cstack, dict) }
  where xs = tail words

repl :: ForthState -> IO ForthState
repl state =
  do putStr "> " ;
     input <- getLine
     nustate <- eval (words input) state
     repl nustate

main = do
  putStrLn "Welcome to your forth interpreter!"
  repl (initialIStack, initialCStack, dictionary) 

showAll []     = ""
showAll (x:xs) = (show x)++" "++(showAll xs)

dup []          = []
dup all@(x:xs)  = x:all

dropF []         = []
dropF all@(x:xs) = xs

swap []         = []
swap all@(x:y:xs) = y:x:xs

rot []         = []
rot all@(x:y:z:xs) = z:x:y:xs

stA all@(x:y:xs) = if (x > y) then -1 else 0

gtA all@(x:y:xs) = if (x < y) then -1 else 0

eqA all@(x:y:xs) = if (x == y) then -1 else 0

condition (i:istack) xs = if (i == -1) then xs
                          else (indexOf xs "else")

conditionN (i:istack) xs = if (i == 0) then xs
                           else []

indexOf [] word = [] 
indexOf (x:xs) word = if (x == word || x == ";") then xs
                      else (indexOf xs word) 

untilV [] word     = []
untilV (x:xs) word = if (x /= word) then x:(untilV xs word)
                    else []