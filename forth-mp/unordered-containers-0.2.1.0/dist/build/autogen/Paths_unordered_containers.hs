module Paths_unordered_containers (
    version,
    getBinDir, getLibDir, getDataDir, getLibexecDir,
    getDataFileName
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
catchIO = Exception.catch


version :: Version
version = Version {versionBranch = [0,2,1,0], versionTags = []}
bindir, libdir, datadir, libexecdir :: FilePath

bindir     = "/Users/felipefreire/Library/Haskell/ghc-7.4.1/lib/unordered-containers-0.2.1.0/bin"
libdir     = "/Users/felipefreire/Library/Haskell/ghc-7.4.1/lib/unordered-containers-0.2.1.0/lib"
datadir    = "/Users/felipefreire/Library/Haskell/ghc-7.4.1/lib/unordered-containers-0.2.1.0/share"
libexecdir = "/Users/felipefreire/Library/Haskell/ghc-7.4.1/lib/unordered-containers-0.2.1.0/libexec"

getBinDir, getLibDir, getDataDir, getLibexecDir :: IO FilePath
getBinDir = catchIO (getEnv "unordered_containers_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "unordered_containers_libdir") (\_ -> return libdir)
getDataDir = catchIO (getEnv "unordered_containers_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "unordered_containers_libexecdir") (\_ -> return libexecdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
