data Tree a = Node a (Tree a) (Tree a)
            | Empty 

instance (Show a) => Show (Tree a) where
       show t = aux t 0
         where aux Empty level = times level "+--" ++ "X\n"
               aux (Node v Empty Empty) level =
                   times level "+--" ++ (show v) ++ "\n"
               aux (Node v l r) level =
                   times level "+--" ++ (show v) ++ "\n" ++
                   aux l (level+1) ++
                   aux r (level+1)
               times num elt = concat (take num (repeat elt))

instance Functor Tree where
        fmap f Empty = Empty
        fmap f (Node a l r) = Node (f a)
                                   (fmap f l)
                                   (fmap f r)

add Empty elt = Node elt Empty Empty
add nod@(Node x vl vr) elt
    | elt < x   = Node x (add vl elt) vr
    | otherwise = Node x vl (add vr elt)

left nod@(Node x Empty Empty) = nod
left nod@(Node x vl Empty) = nod
left nod@(Node x Empty vr@(Node r vlr vrr)) = Node r (nod) Empty
left nod@(Node x vl@(Node l vll vrl) vr@(Node r vlr vrr)) = Node r (Node x vl vlr) (vrr) 

doubleleft nod@(Node x vl vr) = left (Node x vl (right vr))

right nod@(Node x Empty Empty) = nod
right nod@(Node x Empty vr) = nod
right nod@(Node x vl@(Node l vll vrl) Empty) = Node l Empty (nod)
right nod@(Node x vl@(Node l vll vrl) vr@(Node r vlr vrr)) = Node l (vll) (Node x vrl vr)

doubleright nod@(Node x vl vr) = right (Node x (left vl) vr)

search Empty elt = Empty
search nod@(Node x vl vr) elt
      | elt < x   =  (search vl elt)
      | elt > x   =  (search vr elt)
      | otherwise =  nod

