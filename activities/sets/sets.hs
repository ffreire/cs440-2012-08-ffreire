-- Sets activity

member :: (Ord a) => [a] -> a -> Bool
member [] x = False
member (y:ys) x = if x == y then True else member ys x

add :: (Ord a) => a -> [a] -> [a]
add x xs = if not (member xs x) then quicksort(x:xs) else xs

union' :: (Ord a) => [a] -> [a] -> [a]
union' xx [] = xx
union' xx (x:xs) = if not (member xx x)
                         then quicksort (union' (x:xx) xs)
                         else union' xx xs

intersection :: (Ord a) => [a] -> [a] -> [a]
intersection xx [] = [] 
intersection xx (x:xs) = if (member xx x)
                            then quicksort(x:(intersection xx xs))
                            else intersection xx xs

quicksort :: (Ord a) => [a] -> [a]
quicksort [] = []
quicksort (x:xs) = 
      let smallerSorted = quicksort (filter (<=x) xs)
          biggerSorted = quicksort (filter (>x) xs)
      in smallerSorted ++ [x] ++ biggerSorted

powerset :: (Ord a) => [a] -> [[a]]
powerset [] = [[]]
powerset (x:xs) = powerset xs ++ (map (++[x]) (powerset xs))

fib 0 = 0
fib 1 = 1
fib x = fib (x-1) + fib (x-2)
