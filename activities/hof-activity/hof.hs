mymap :: (t->a) -> [t] -> [a]
mymap f [] = []
mymap f (x:xs) = f(x): mymap f xs

flatmap :: (t -> [a]) -> [t] -> [a]
flatmap f [] = []
flatmap f (x:xs) = f(x) ++ flatmap f xs

myfoldr :: (t1 -> t -> t) -> t -> [t1] -> t
myfoldr f b [] = b
myfoldr f b (x:xs) = f x (myfoldr f b xs)

myfoldl :: (t -> t1 -> t) -> t -> [t1] -> t
myfoldl f b [] = b
myfoldl f b (x:xs) = f (myfoldl f b xs) x

myzipwith :: (a -> b -> c) -> [a] -> [b] -> [c]
myzipwith f _ [] = []
myzipwith f (x:xs) (y:ys) = [f x y] ++ (myzipwith f xs ys)

exists :: (t -> Bool) -> [t] -> Bool
exists f [] = False
exists f (x:xs) = if f x then True else exists f xs

forall :: (t -> Bool) -> [t] -> Bool
forall f [] = True
forall f (x:xs) = if f x then forall f xs else False

myfilter :: (a -> Bool) -> [a] -> [a]
myfilter f [] = []
myfilter f (x:xs) = if f x then x:(myfilter f xs) else (myfilter f xs)

inclist :: (Num a) => [a] -> [a]
inclist xx = mymap (+1) xx

doublelist :: [Integer] -> [Integer]
doublelist xx = mymap (*2) xx

sumlist :: [Integer] -> Integer
sumlist xx = myfoldr (+) 0 xx

prodlist :: [Integer] -> Integer
prodlist xx = myfoldr (*) 1 xx

sumpairs :: (Num a) => [a] -> [a] -> [a]
sumpairs xx yy = myzipwith (+) xx yy

intersect :: (Eq a) => [a] -> [a] -> [a]
intersect xx yy = [ a | a<-xx, elem a yy]

prefix :: a -> [[a]] -> [[a]]
prefix x xx = [ x:a | a<-xx]

pow :: [a] -> [[a]]
pow [] = [[]]
pow (x:xs) = pow xs ++ (prefix x (pow xs))

nats :: [Integer]
nats = [ a | a<-[1..]]
